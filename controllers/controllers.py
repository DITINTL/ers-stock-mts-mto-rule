# -*- coding: utf-8 -*-
from odoo import http


class MtoMtsByErs(http.Controller):
    @http.route('/mto_mts_by_ers/mto_mts_by_ers/', auth='public')
    def index(self, **kw):
        return "Hello, world"

    @http.route('/mto_mts_by_ers/mto_mts_by_ers/objects/', auth='public')
    def list(self, **kw):
        return http.request.render('mto_mts_by_ers.listing', {
            'root': '/mto_mts_by_ers/mto_mts_by_ers',
            'objects': http.request.env['mto_mts_by_ers.mto_mts_by_ers'].search([]),
        })

    @http.route('/mto_mts_by_ers/mto_mts_by_ers/objects/<model("mto_mts_by_ers.mto_mts_by_ers"):obj>/', auth='public')
    def object(self, obj, **kw):
        return http.request.render('mto_mts_by_ers.object', {
            'object': obj
        })
